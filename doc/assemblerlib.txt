The assembler library contains everything you need to convert a table of 
assembly lines into a table, that you can feed into the emulator.

Use the assembler.lua frontend for an example on how to use this library

require("tisassemblerlib") eturns a table containing
Assembler() a constructor for a blank assembler class
TisNodeAssembler() a constructor for an assembler, that assembles 
                   instructions for the Tis Node
The assembler classes have the following functions:
assembler:assemble(table:lines) -> result,reason
assembler:addPattern(string:pattern,function(symbols,...):assemble,number/function(symbols,...):size)

USING THE ASSEMBLER

call the assemble function with a table containg a string for each line,
if it assembles successfully the function will return a table mapping addresses to instructions
if an error occours the assemble function will return false and an error message

EXPANDING THE ASSEMBLER

The addPattern function is used for expanding the assemblers functionality
Letters and numbers are matched directly, spaces " " represent any amount of tabs or spaces
A tilde "~" is used to represent an argument.
The arguments are matched and passed in order to the assemble and size functions
the assemble callback function thakes the above described arguments and 
returns a table of instructions the instructions MUST be the same amount as 
specified in the size parameter or returned by the size callback function
if the actual size is not the same as the estimated size the labels will be off without
an error message

The symbols table contains the following parameters
 symbols[0] the last label, that did not start with a "."
 symbols.labels stores addresses of all labels
 symbols.labels["$"] stores the addess at wich the next instruction will be placed
 symbols.defines stores all values defined by the #DEFINE


-------------------------------------------
--- DEBUG IO PRINTER --------------------
---------------------------------------

function tisDebugPrinterWrite(this,value)
	print(this.prefix..string.format("0x%04X",value))
	return true
end

function tisDebugPrinterCanWrite(this)
	return true
end

function TisDebugPrinter(prefix)
	this = {}
	this.type = "C90"
	this.prefix = prefix or ""
	this.canWrite = tisDebugPrinterCanWrite
	this.write = tisDebugPrinterWrite
	return this
end

return TisDebugPrinter

local assemblerlib = require("tislib.assembler")
local Assembler = assemblerlib.Assembler
local toNumber = assemblerlib.toNumber
local encodeRegOp = assemblerlib.encodeRegOp
local registerOrError = assemblerlib.registerOrError
local getNumberOrSymbolOrError = assemblerlib.getNumberOrSymbolOrError

function printDummy() end
local print = printDummy --disable debug output

local X21registers = {
	NIL=0,
	ACC=1,
	IMM=6,
	UP=8,
	LEFT=9,
	DOWN=10,
	RIGHT=11,
	ANY=12,
	LAST=13,
}

function encodeAccOP(this,x,f,o)
	op = f << 4 | o << 12
	local num = toNumber(x)
	if num and num ~= 0 then
		local lop = (num&0xFFF0) >> 4
		op = op | (num&0x000F)
		op = op | (0x6<<8)
		return {lop,op}
	else
		local reg = registerOrError(this,x)
		op = op | (reg<<8)
		return {op}
	end
end

function a1ADD(this,x) return encodeAccOP(this,x,0,1) end
function a1SUB(this,x) return encodeAccOP(this,x,1,1) end
function a1MUL(this,x) return encodeAccOP(this,x,2,1) end
function a1DIV(this,x) return encodeAccOP(this,x,3,1) end
function a1AND(this,x) return encodeAccOP(this,x,4,1) end
function a1OR(this,x)  return encodeAccOP(this,x,5,1) end
function a1XOR(this,x) return encodeAccOP(this,x,6,1) end
function a1SHL(this,x) return encodeAccOP(this,x,7,1) end
function a1SHR(this,x) return encodeAccOP(this,x,8,1) end

function getAccOpSize(this,x)
	local num = toNumber(x)
	if num and num ~= 0 then
		return 2
	else
		return 1
	end
end

function encode2Op(this,f)
	local op = 0x2000 | (f<<4)
	return {op}
end

function a0SAV(this) return encode2Op(this,0) end
function a0SWP(this) return encode2Op(this,1) end
function a0NOT(this) return encode2Op(this,2) end
function a0NEG(this) return encode2Op(this,3) end
function a0RRLAST(this) return encode2Op(this,4) end
function a0RLLAST(this) return encode2Op(this,5) end

function a2MOV(this,x,r)
	print("MOV",x,r)
	local reg = registerOrError(this,r)
	return encodeAccOP(this,x,reg,3)
end

function a2MOVSize(this,x,r)
	return getAccOpSize(this,x)
end

function encodeJMPOp(this,c,adr)
	local addr = getNumberOrSymbolOrError(this,adr)
	if addr > 0xff then error(string.format("Address %s = 0x%x is out of bounds",adr,addr)) end
	local op = 0x4000 | c<<8 | addr
	return {op}
end

function a1JMP(this,adr) return encodeJMPOp(this,0,adr) end
function a1JEZ(this,adr) return encodeJMPOp(this,1,adr) end
function a1JNZ(this,adr) return encodeJMPOp(this,2,adr) end
function a1JLZ(this,adr) return encodeJMPOp(this,3,adr) end
function a1JGZ(this,adr) return encodeJMPOp(this,4,adr) end

function a1JRO(this,x)
	local op = toNumber(x)
	local reg = 6
	if not op then
		reg = registerOrError(x)
		op = 0
	end
	op = (op&0xFF) | 0x5000 | reg<<8
	return {op}
end

function a0NOP(this)
	return a1ADD(this,"NIL")
end

function a0HLT(this)
	return a1JRO(this,"NIL")
end

function TisX21Assembler()
	local assembler = Assembler()
	assembler.registers = X21registers
	assembler:addPattern("NOP",a0NOP,1) --do nothing
	assembler:addPattern("HLT",a0NOP,1)
	assembler:addPattern("ADD ~",a1ADD,getAccOpSize)
	assembler:addPattern("SUB ~",a1SUB,getAccOpSize)
	assembler:addPattern("MUL ~",a1MUL,getAccOpSize)
	assembler:addPattern("DIV ~",a1DIV,getAccOpSize)
	assembler:addPattern("SHL ~",a1SHL,getAccOpSize)
	assembler:addPattern("SHR ~",a1SHR,getAccOpSize)
	assembler:addPattern("AND ~",a1AND,getAccOpSize)
	assembler:addPattern("OR ~", a1OR ,getAccOpSize)
	assembler:addPattern("XOR ~",a1XOR,getAccOpSize)
	assembler:addPattern("SAV",a0SAV,1)
	assembler:addPattern("SWP",a0SWP,1)
	assembler:addPattern("NEG",a0NEG,1)
	assembler:addPattern("NOT",a0NOT,1)
	assembler:addPattern("RRLAST",a0RRLAST,1)
	assembler:addPattern("RLLAST",a0RLLAST,1)
	assembler:addPattern("JMP ~",a1JMP,1)
	assembler:addPattern("JEZ ~",a1JEZ,1)
	assembler:addPattern("JNZ ~",a1JNZ,1)
	assembler:addPattern("JLZ ~",a1JLZ,1)
	assembler:addPattern("JGZ ~",a1JGZ,1)
	assembler:addPattern("JRO ~",a1JRO,1)
	assembler:addPattern("MOV ~ ~",a2MOV,a2MOVSize)
	return assembler
end

return TisX21Assembler

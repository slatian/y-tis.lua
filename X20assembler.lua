local TisNodeAssembler = require("tislib.X20assembler")
local disassembler = require("tislib.X20disassembler")

local assembler = TisNodeAssembler()

--test setup
local lines = {}
while true do
	i = io.read()
	if not i then break end
	lines[#lines+1] = i
end

res,reason = assembler:assemble(lines,true)
if not res then
	print(reason)
else
	print("--X20")
	i = 0
	while i < 0xFFFF do
		local v = res[i]
		if v then
			print(string.format("[0x%04X]=0x%04x, -- %s",i,v,disassembler.disassembleSingleInstruction(v)))
		end
		i=i+1
	end
end


-------------------------------------------
--- IO FIFO BUFFER ----------------------
---------------------------------------

--returns a value or nil
function tisFifoRead(this)
	return table.remove(this.storage,1)
end

--attempts to write a value to the fifo
--returns true on success, false on failure
function tisFifoWrite(this,value)
	local size = #this.storage
	if size >= this.size then return false end
	this.storage[size+1] = value
	return true
end

function tisFifoCanWrite(this)
	return #this.storage < this.size
end

function tisFifoCanRead(this)
	return #this.storage > 0
end

--returns the amount of values
function tisFifoGetValueCount(this)
	return #this.storage
end

function tisFifoRenderUtf8(this,...)
	local tisNodeRenderUtf8 = require("tislib.C3x_toUtf8")
	this.renderUtf8 = tisNodeRenderUtf8
	return tisNodeRenderUtf8(this,...)
end

--creates a Tis IO register
function TisFifo(size)
	local fifo = {}
	fifo.type = "C31"
	fifo.storage = {}
	fifo.size = size or 1
	fifo.read = tisFifoRead
	fifo.write = tisFifoWrite
	fifo.canWrite = tisFifoCanWrite
	fifo.canRead = tisFifoCanRead
	fifo.getValueCount = tisFifoGetValueCount
	fifo.renderUtf8 = tisFifoRenderUtf8
	return fifo
end

return TisFifo

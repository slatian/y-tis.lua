local function printDummy() end
local print = printDummy --disable debug output

--size may be a number or a function wich takes the pattern match as input
local function addPattern(this,pattern,callback,size)
	pattern = pattern:gsub("+","%%+")
	pattern = pattern:gsub("-","%%-")
	pattern = pattern:gsub(" ","%%s+")
	pattern = pattern:gsub("~","([^%%s,]+)")
	this.patterns["^%s*"..pattern.."%s*$"] = {cb=callback,size=size}
end

local function stripComments(lines)
	for k,line in pairs(lines) do
		lines[k] = line:match("^([^;]*)")
	end
end

local function splitLine(line)
	if line:match(":") then
		return line:match("%s*([%a%d%._]*):%s*(.*)")
	else
		return nil,line
	end
end

local function toNumber(token)
	if type(token) == "number" then
		return token
	end
	if token:match("^-?%d+$") then
		return tonumber(token)
	elseif token:match("^0x%x+$") then
		return tonumber(token:sub(3),16)
	elseif token:match("^0b[01]+") then
		return tonumber(token:sub(3),2)
	elseif token:match("^0o[01234567]+") then
		return tonumber(token:sub(3),8)
	else
		return nil
	end
end

local function getCommandAssembledSize(this,command)
	for pattern,v in pairs(this.patterns) do
		if command:match(pattern) then
			print(" Command matches pattern: "..pattern)
			if type(v.size) == "number" then
				return v.size
			elseif type(v.size) == "function" then
				return v.size(this,command:match(pattern))
			else
				return 1
			end
		end
	end
	return 0
end

local function assembleCommand(this,command,symbols)
	for pattern,v in pairs(this.patterns) do
		if command:match(pattern) then
			print(" Command matches pattern: "..pattern)
			local t = {
				registers=this.registers,
				symbols=symbols,
			}
			return v.cb(t,command:match(pattern))
		end
	end
	error("Unable to make sense of this command: "..tostring(command))
end

local function assemble(this,inputlines)
	stripComments(inputlines)
	local symbols = {labels={["$"]=0},defines={}}
	--symbols[0] contains the name of last non sub label
	--symbols.labels["$"] contains the address at wich the next instruction will be placed
	local i = 1
	while i < #inputlines do
		local line = inputlines[i]
		print("Preprocessing line "..tostring(i).." ["..string.format("0x%04X",symbols.labels["$"]).."]: "..tostring(line))
		local label,command = splitLine(line)
		if label then
			print(" Label: "..label)
			if label:sub(1,1) == "." then
				label = (symbols[0] or "")..label
				print("  -> "..label)
			else
				symbols[0] = label
				print("  new namespace")
			end
			if symbols.labels[label] then
				return false,tostring(i)..": Label got redefined: "..label
			else
				symbols.labels[label] = symbols.labels["$"]
			end
		end
		if command:match("^%s*$") then
			--blank command do nothing
		elseif command:match("^%s*#DEFINE%s+%w+%s+%w+%s*$") then
			local key,value = command:match("^%s*#DEFINE%s+(%w+)%s+(%w+)%s*$")
			print(" define "..key.." = "..value)
			local num = toNumber(value)
			if num then value = num print("value is a number: "..tostring(num)) end
			symbols.defines[key] = value
		elseif command:match("^%s*#ORG%s+%w+%s*$") then
			local addr = command:match("^%s*#ORG%s+(%w+)%s*$")
			local num = toNumber(addr)
			if not num then return false,tostring(i)..": The address in an #ORG directive has to be a number" end
			print(string.format(" org 0x%04X",num))
			symbols.labels["$"] = num
		else
			local off = getCommandAssembledSize(this,command)
			symbols.labels["$"] = symbols.labels["$"] + off
		end
		i=i+1
	end
	local output = {}
	symbols[0] = nil
	symbols.labels["$"] = 0
	i = 1
	while i < #inputlines do
		local line = inputlines[i]
		print("Assembling line "..tostring(i).." ["..string.format("0x%04X",symbols.labels["$"]).."]: "..tostring(line))
		local label,command = splitLine(line)
		if label then
			if not (label:sub(1,1) == ".") then
				symbols[0] = label
				print("Namespace: "..label)
			end
		end
		if command:match("^%s*$") then
			--blank line do nothing
		elseif command:match("^%s*#DEFINE%s+%w+%s+%w+%s*$") then
		elseif command:match("^%s*#ORG%s+%w+%s*$") then
			local addr = command:match("^%s*#ORG%s+(%w+)%s*$")
			local num = toNumber(addr)
			if not num then return false,tostring(i)..": The address in an #ORG directive has to be a number" end
			symbols.labels["$"] = num
		else
			print(" Assembling command: "..command)
			local ok,res = false,false
			if print == printDummy then
				ok,res = pcall(assembleCommand,this,command,symbols)
			else
				res = assembleCommand(this,command,symbols) ok=true
			end
			if not ok then return false,tostring(i)..": "..tostring(res) end
			local off = 0
			local addr = symbols.labels["$"]
			while off < #res do
				output[addr+off] = res[off+1]
				off = off+1
			end
			symbols.labels["$"] = addr + off
		end
		i=i+1
	end
	return output
end

local function Assembler()
	this = {}
	this.registers = {}
	this.patterns = {}
	this.addPattern = addPattern
	this.assemble = assemble
	return this
end

local function getSymbolName(this,token)
	if token:sub(1,1) == "." then
		return (this.symbols[0] or "")..token
	else
		return token
	end
end

local function getDefine(this,token)
	return this.symbols.defines[getSymbolName(this,token)]
end

local function getLabel(this,token)
	return this.symbols.labels[getSymbolName(this,token)]
end

local function getNumberOrSymbolOrError(this,token)
	print("getNumberOrSymbolOrError(this,"..tostring(token)..")")
	local num = toNumber(token)
	if num then return num end
	num = getDefine(this,token)
	if type(num) == "number" then return num end
	if type(num) == "string" then num = getLabel(this,num) end
	if num == nil then num = getLabel(this,token) end
	if num == nil then error("Undefined Symbol: "..tostring(token)) end
	return num
end

local function getNumberOrDefineOrNil(this,token)
	local num = toNumber(token)
	if num then return num end
	num = getDefine(this,token)
	if type(num) == "number" then return num end
	return nil
end


local function getRegister(this,token,rec)
	print("getRegister(this,"..tostring(token)..")")
	for k,v in pairs(this) do print(k,v) end
	local reg = this.registers[token:upper()]
	if token.sub(1,1) == "/" then
		return toNumber(token:sub(2))
	end
	if (not reg) and toNumber(token) == 0 then reg = this.registers.NIL end
	if not reg and rec then return nil end --no more than one recursion
	if not reg then
		local name = getDefine(this,token)
		if not name then return nil end
		return getRegister(this,name,true)
	end
	if reg > 0xF then return nil end
	return reg
end

local function registerOrError(this,reg)
	print("registerOrError(this,"..tostring(reg)..")")
	local regn = getRegister(this,reg)
	print(this.registers)
	if not regn then error("No such register: "..tostring(reg)) end
	return regn
end

local function encodeRegOp(this,r,x,y,f)
	print("encodeRegOp(this,"..tostring(r)..","..tostring(x)..","..tostring(y)..","..tostring(f)..")")
	local rreg = registerOrError(this,r)
	local xreg = registerOrError(this,x)
	local yreg = registerOrError(this,y)
	local op = f<<12
	op = op | rreg<<8
	op = op | xreg<<4
	op = op | yreg
	return {op}
end

return {
	Assembler = Assembler,
	toNumber = toNumber,
	encodeRegOp = encodeRegOp,
	registerOrError = registerOrError,
	getSymbolName=getSymbolName,
	getDefine=getDefine,
	getLabel=getLabel,
	getNumberOrSymbolOrError=getNumberOrSymbolOrError,
	getNumberOrDefineOrNil=getNumberOrDefineOrNil,
	getRegister=getRegister,
}

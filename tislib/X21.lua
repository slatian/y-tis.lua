-------------------------------------------
--- TIS X21 NODE ------------------------
---------------------------------------

--registers 2 to 5, 7, 14 and 15 are reserved
--register 0 is NIL
--register 1 is a 16-bit general purpose register
--registers 8-13 are io registers

function tisNodeCanReadRegister(this,register)
	if register < 0 then return false end
	if register < 2 then return true end
	if register == 6 then return true end
	if register >= 8 and register <= 11 then
		local input = this.inputs[register&0x7]
		if input then
			return input:canRead()
		end
	end
	if register == 12 then --ANY
		for i=1,4 do
			local input = this.inputs[i]
			if input then
				if input:canRead() then
						return true
				end
			end
		end
		if register == 13 then --LAST
			if not this.last then return false end
			local input = this.inputs[this.last&0x3]
				if input then
					return input:canRead()
				end
		end
		return false
	end
	return false
end

function tisNodeCanWriteRegister(this,register)
	if register < 0 then return false end
	if register < 2 then return true end
	if register == 6 then return true end
	if register >= 8 and register <= 11 then
		local output = this.outputs[register&0x7]
		if output then
			return output:canWrite()
		end
	end
	if register == 12 then --ANY
		for i=1,4 do
			local output = this.outputs[i]
			if output then
				if output:canWrite() then
						return true
				end
			end
		end
		if register == 13 then --LAST
			if not this.last then return false end
			local output = this.outputs[this.last&0x3]
				if output then
					return output:canWrite()
				end
		end
		return false
	end
	return false
end

--returns value or nil
function tisNodeReadRegister(this,register)
	if register < 0 then return nil end
	if register == 0 then return 0 end
	if register == 1 then return this.acc end
	if register == 6 then return this.imm end
	if register >= 8 and register <= 11 then
		local input = this.inputs[register&0x3]
		if input then
			return (input:read()&0xFFFF)
		end
	end
	if register == 12 then --ANY
		for i=1,4 do
			local input = this.inputs[i]
			if input then
				if input:canRead() then
					local value = input:read()
					if value then
						this.last = i-1
					end
					return value
				end
			end
		end
	end
	if register == 13 then --LAST
		if this.last then
			local input = this.inputs[this.last+1]
			if input then
				if input:canRead() then
						return input:read()
				end
			end
		end
	end
	return nil
end

-- returns true on success
function tisNodeWriteRegister(this,register,value)
	value = value & 0xFFFF
	if register < 0 then return false end
	if register == 0 then return true end
	if register == 1 then this.acc = value return true end
	if register >= 8 and register <= 11 then
		local output = this.outputs[register&0x3]
		if output then
			return output:write(value)
		end
	end
	if register == 12 then --ANY
		for i=1,4 do
			local output = this.outputs[i]
			if output then
				if output:canWrite() then
					if output:write(value) then
						this.last = i-1
						return true
					else
						return false
					end
				end
			end
		end
	end
	if register == 13 then --LAST
		if this.last then
			local output = this.outputs[this.last+1]
			if output then
				if output:canWrite() then
						return output:write()
				end
			end
		end
	end
	return false
end

function tisNodeSetWait(this,register,write)
	this.wait = true
	tis.waitingFor = register
	tis.waitForWrite = write or false
end

function tisNodeMUL(this,x)
	this.acc = x*this.acc
end

function tisNodeDIV(this,x)
	if x == 0 then
		this:reset()
	else
		this.acc = this.acc//x
	end
end

function tisNodeAND(this,x)
	this.acc = x&this.acc
end

function tisNodeOR(this,x)
	this.acc = x|this.acc
end

function tisNodeXOR(this,x)
	this.acc = x~this.acc
end

function tisNodeSHL(this,a)
	this.acc = this.acc<<a
end

function tisNodeSHR(this,a)
	this.acc = this.acc>>a
end

function tisNodeSUB(this,x)
	this.acc = this.acc-x
end

function tisNodeADD(this,x)
	this.acc = this.acc+x
end


local node1functions = {
	[0] = tisNodeADD,
	[1] = tisNodeSUB,
	[2] = tisNodeMUL,
	[3] = tisNodeDIV,
	[4] = tisNodeAND,
	[5] = tisNodeOR,
	[6] = tisNodeXOR,
	[7] = tisNodeSHL,
	[8] = tisNodeSHR,
}

function tisNode1(this,x,o,i)
	if not tisNodeCanReadRegister(this,x) then tisNodeSetWait(this,x) return end
	this.imm = this.imm | i
	local f = node1functions[o]
	if not f then tisNodeInvalidOPC(this) return end
	f(this,tisNodeReadRegister(this,x))
	this.acc = this.acc&0xFFFF
	this.pc = this.pc+1
	this.imm = 0
end

function tisNode2(this,u,o,v)
	if not u == 0 then tisNodeInvalidOPC(this) return end
	if not v == 0 then tisNodeInvalidOPC(this) return end
	if o == 0 then --SAV
		this.bak = this.acc
	elseif o == 1 then --SWP
		local bak = this.bak
		this.bak = this.acc
		this.acc = bak
	elseif o == 2 then --NOT
		this.acc = ~this.acc		
	elseif o == 3 then --NEG
		this.acc = (~this.acc+1)&0xFFFF
	elseif o == 4 then --RRLAST
		if this.last then
			this.last = (this.last-1)&3
		end
	elseif o == 5 then --RLLAST
		if this.last then
			this.last = (this.last+1)&3
		end
	else
		tisNodeInvalidOPC(this) return
	end
	this.pc = this.pc+1
end

function tisNodeLDI(this,x,y,z)
	this.imm = (x<<12) | (y<<8) | (z<<4)
	this.pc = this.pc+1
end

function tisNodeMOV(this,x,y,i)
	if not tisNodeCanReadRegister(this,x) then tisNodeSetWait(this,x) return end
	if not tisNodeCanWriteRegister(this,y) then tisNodeSetWait(this,y,true) return end
	this.imm = this.imm | i
	tisNodeWriteRegister(this,y,tisNodeReadRegister(this,x))
	this.imm = 0
	this.pc = this.pc+1
end

function tisNodeJMP(this,c,a2,a1)
	if c > 4 then tisNodeInvalidOPC(this) return end
	local adr = a2<<4 | a1
	if c == 0 then this.pc = adr end --JMP
	if c == 1 and this.acc == 0 then this.pc = adr end --JEZ
	if c == 2 and this.acc ~= 0 then this.pc = adr end --JNZ
	if c == 3 and this.acc >= 0x8000 then this.pc = adr end --JLZ
	if c == 4 and this.acc <= 0x7FFF and acc ~= 0 then this.pc = adr end --JGZ
end

function tisNodeJRO(this,x,i2,i1)
	this.imm = i2<<4 | i1
	if this.imm > 0x7F then this.imm = this.imm | 0xFF00 end
	if not tisNodeCanReadRegister(this,x) then tisNodeSetWait(this,x) return end
	local off = tisNodeReadRegister(this,x)
	if off == 0 and x < 8 then
		this.halt = true	
		return
	end
	--print(("JRO 0x%04x , %d"):format(off,((~off)&0xFFFF)+1))
	if off > 0x7FFF then 
		-- seek backward
		off = ((~off)&0xFFFF)+1
		for i=1,off do
			this.pc = (this.pc-1)&0xFF
			while (this.program[this.pc-1] or 0)&0xF000 == 0 do
				if not this.program[this.pc] then
					if this.pc == 0 then break end
				end
				this.pc = (this.pc-1)&0xFF
			end
		end
	else
		--seek forward
		for i=1,off do
			this.pc = (this.pc+1)&0xFF
			while (this.program[this.pc] or 0)&0xF000 == 0 do
				if not this.program[this.pc] then
					if this.pc == 0 then break end
					this.pc = 0
				else
					this.pc = (this.pc+1)&0xFF
				end
			end
		end
	end	
end

function tisNodeInvalidOPC(this,...)
	this.halt = true
	this.invalidOpc = true
end

local tisNodeOpcodeFunctionTable = {
	[0x0] = tisNodeLDI,
	[0x1] = tisNode1,
	[0x2] = tisNode2,
	[0x3] = tisNodeMOV,
	[0x4] = tisNodeJMP,
	[0x5] = tisNodeJRO,
	[0x6] = tisNodeInvalidOPC,
	[0x7] = tisNodeInvalidOPC,
	[0x8] = tisNodeInvalidOPC,
	[0x9] = tisNodeInvalidOPC,
	[0xA] = tisNodeInvalidOPC,
	[0xB] = tisNodeInvalidOPC,
	[0xC] = tisNodeInvalidOPC,
	[0xD] = tisNodeInvalidOPC,
	[0xE] = tisNodeInvalidOPC,
	[0xF] = tisNodeInvalidOPC,
}

function tisNodeTick(this)
	if this.halt then return end
	if not this.wait then
		this.ir = this.program[this.pc] or 0x4000 --JMP NIL
	end
	this.wait = false
	local opc = (this.ir & 0xF000) >> 12
	local r = (this.ir & 0x0F00) >> 8
	local x = (this.ir & 0x00F0) >> 4
	local y = this.ir & 0x000F
	local func = tisNodeOpcodeFunctionTable[opc]
	func(this,r,x,y)
end

function tisNodeReset(this)
	this.wait = false
	this.halt = false
	this.invalidOpc = false
	this.waitingFor = 0
	this.waitForWrite = false
	this.ir = 0
	this.pc = 0
	this.acc = 0
	this.bak = 0
	this.last = nil
	this.imm = 0
end

function tisNodeRenderUtf8(this,...)
	local tisNodeRenderUtf8 = require("tislib.X21_toUtf8")
	this.renderUtf8 = tisNodeRenderUtf8
	return tisNodeRenderUtf8(this,...)
end

--UP = 0
--LEFT = 1
--DOWN = 2
--RIGHT = 3

--creates a tis node
function TisNode(program,inputs,outputs)
	local tis = {}
	tis.type = "X21"
	tis.program = program or {}
	tis.pc = 0 --program counter (stores next instruction address)
	tis.ir = 0 --instruction register
	tis.imm = 0
	tis.acc = 0
	tis.bak = 0
	tis.last = nil
	tis.wait = false
	tis.halt = false
	tis.invalidOpc = false
	tis.waitingFor = 0
	tis.waitForWrite = false
	tis.inputs = inputs or {}
	tis.outputs = outputs or {}
	tis.tick = tisNodeTick
	tis.reset = tisNodeReset
	tis.renderUtf8 = tisNodeRenderUtf8
	return tis
end

return TisNode

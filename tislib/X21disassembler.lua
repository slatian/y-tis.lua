function ildi(instruction)
	return ("LDI 0x%03x"):format(instruction&0xFFF)
end

local accOps = {
 [0] = "ADD",
 [1] = "SUB",
 [2] = "MUL",
 [3] = "DIV",
 [4] = "AND",
 [5] = "OR",
 [6] = "XOR",
 [7] = "SHL",
 [8] = "SHR",
}

local registers = {
	[0] = "NIL",
	[1] = "ACC",
	[2] = "/2",
	[3] = "/3",
	[4] = "/4",
	[5] = "/5",
	[6] = "IMM",
	[7] = "/7",
	[8] = "UP",
	[9] = "LEFT",
	[10] = "DOWN",
	[11] = "RIGHT",
	[12] = "ANY",
	[13] = "LAST",
	[14] = "/14",
	[15] = "/15",
}

function getRegister(r,imm)
	local reg = registers[r]
	if reg == "IMM" and imm > 0 then reg = string.format("IMM+0x%x",imm) end
	return reg
end

function iAccOp(instruction)
	local inst = accOps[(instruction&0x00F0)>>8]
	local imm = instruction&0x000F
	local reg = getRegister((instruction&0x0F00)>>8,imm)
	return inst.." "..reg
end

local noArgOps = {
	[0] = "SAV",
	[1] = "SWP",
	[2] = "NOT",
	[3] = "NEG",
	[4] = "RRLAST",
	[5] = "RLLAST",
}

function iNoArgOp(instruction)
	return noArgOps[(instruction&0x00F0)>>4]
end

function imov(instruction)
	local imm = instruction&0x000F
	local src = getRegister((instruction&0x0F00)>>8,imm)
	local dest = registers[(instruction&0x00F0)>>4]
	return "MOV "..src.." "..dest
end

function jump(instruction,jinst,customimmformat)
	local imm = instruction&0xFF
	local reg = registers[(instruction&0x0F00)>>8] or (customimmformat or "0x%02x"):format(imm)
	return ("%s 0x%02x"):format(jinst,imm)
end

function ijmp(instruction) return jump(instruction,"JMP") end
function ijez(instruction) return jump(instruction,"JEZ") end
function ijnz(instruction) return jump(instruction,"JNZ") end
function ijlz(instruction) return jump(instruction,"JLZ") end
function ijgz(instruction) return jump(instruction,"JGZ") end

function ijro(instruction) 
	local imm = instruction&0xFF
	if imm > 0x7F then imm=-(((~imm)&0xFF)+1) end
	local reg = registers[(instruction&0x0F00)>>8] or tostring(imm)
	if reg == "IMM" then reg = tostring(imm) end
	return "JRO "..reg
end


local decoders = {
	--{mask,match,callback},
	{0xFFF0,0x1000,"NOP"},
	{0xFF00,0x5000,"HLT"},
	{0xFFFF,0x5600,"HLT"},
	{0xF000,0x0000,ildi},
	{0xFF00,0x1000,iAccOp},
	{0xFF00,0x1100,iAccOp},
	{0xFF00,0x1200,iAccOp},
	{0xFF00,0x1300,iAccOp},
	{0xFF00,0x1400,iAccOp},
	{0xFF00,0x1500,iAccOp},
	{0xFF00,0x1600,iAccOp},
	{0xFF00,0x1700,iAccOp},
	{0xFF00,0x1800,iAccOp},
	{0xFF00,0x2000,iNoArgOp},
	{0xFF00,0x2100,iNoArgOp},
	{0xFF00,0x2200,iNoArgOp},
	{0xFF00,0x2300,iNoArgOp},
	{0xFF00,0x2400,iNoArgOp},
	{0xFF00,0x2500,iNoArgOp},
	{0xF000,0x3000,imov},
	{0xFF00,0x4000,ijmp},
	{0xFF00,0x4100,ijez},
	{0xFF00,0x4200,ijnz},
	{0xFF00,0x4300,ijlz},
	{0xFF00,0x4400,ijgz},
	{0xF000,0x5000,ijro},
}

function disassembleSingleInstruction(instruction)
	for _,v in pairs(decoders) do
		if instruction & v[1] == v[2] then
			if type(v[3]) == "string" then
				return v[3]
			else
				return v[3](instruction)
			end		
		end
	end
	return string.format("INST /0x%04x",instruction)
end

return {disassembleSingleInstruction = disassembleSingleInstruction}

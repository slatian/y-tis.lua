-------------------------------------------
--- IO STACK BUFFER ---------------------
---------------------------------------

--returns a value or nil
function tisStackRead(this)
	if #this.storage > 0 then
		local value = this.storage[#this.storage]
		this.storage[#this.storage] = nil
		return value
	else
		return nil
	end
end

--attempts to write a value to the fifo
--returns true on success, false on failure
function tisStackWrite(this,value)
	local size = #this.storage
	if size >= this.size then return false end
	this.storage[size+1] = value
	return true
end

function tisStackCanWrite(this)
	return #this.storage < this.size
end

function tisStackCanRead(this)
	return #this.storage > 0
end

--returns the amount of values
function tisStackGetValueCount(this)
	return #this.storage
end

function tisStackRenderUtf8(this,...)
	local tisNodeRenderUtf8 = require("tislib.C3x_toUtf8")
	this.renderUtf8 = tisNodeRenderUtf8
	return tisNodeRenderUtf8(this,...)
end

--creates a Tis IO register
function TisStack(size)
	local stack = {}
	stack.type = "C30"
	stack.storage = {}
	stack.size = size or 1
	stack.read = tisStackRead
	stack.write = tisStackWrite
	stack.canWrite = tisStackCanWrite
	stack.canRead = tisStackCanRead
	stack.getValueCount = tisStackGetValueCount
	stack.renderUtf8 = tisStackRenderUtf8
	return stack
end

return TisStack

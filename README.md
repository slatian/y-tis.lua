# Y-TIS.LUA

A Mix of the RISC V and TIS arcitectures implemented in lua,
complete with assembler and emulator library.

## Concept

If you have played the game TIS-100 you know what this is about

This project is based on simple compute nodes, that communicate trough a simple syncronus binary api.
The base word length is 16-bit. 
The api uses the following functions: canWrite() write() canRead() read()

## Modules

| Type | Class name      | Description                           |
| ---- | --------------- | ------------------------------------- |
| X20  | TisNode         | A simple execution node               |
| C30  | TisStack        | A stack node                          |
| C31  | TisFifo         | A fifo node                           |
| C90  | TisDebugPrinter | Writes values written to it to stdout |

## Programming

To program an X20 Node first write you program in assembly (for a reference look at doc/X20Assembly.md)
then pipe the program into the stdin of assembler.lua, the assembler will output the result to stdout in
a format, that you can copy and paste into a lua table.



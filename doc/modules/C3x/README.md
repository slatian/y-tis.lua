# The C3x Module Series

The C3x Series contains modules, that are connectors and storage at the same time

## Modules

### C30 Stack buffer

The C30 Stack buffer stores all values in a stack meaning the tirst written is the last value read.

The stack starts at storage index 1 growing upward

### C31 Fifo buffer

The C31 Fifo buffer lets you read the values in the same order te values were written to it

The next value read is stored at storage index 1 growing upward

## Common Lua API

All C3x Modules have the following methods:
- read(this) -> number/nil - reads the next value
- canRead(this) -> bool - returns whether the next call to read() will return a value or not
- write(this,value) -> trys to write a value to the module, returns true on success, false on failure
- canWrite(this) -> bool - returns whether the next call to write() will succeed or fail
- getValueCount -> number - returns how many values are stored in the module

All C3x Modules havethe following fields:
- number:size - how many values the module can store at maximum
- storage - a table contining all values in the module (format is module specific)

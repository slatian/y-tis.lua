local canLoadDisassembler,disassembler = pcall(require,"tislib.X20disassembler")
if not canLoadDisassembler then disassembler = nil end

-- width: 24
-- height: 13

function tisNodeRenderUtf8(this,title)
	local status_message = "OK"
	if halt then
		if invalidOpc then status_message = "INVALID OPCODE" 
		else status_message = "HALT" end
	elseif wait then
		status_message = "WAITING FOR "
		if this.waitingFor >= 8 then
			status_message = status_message.."IO"..tostring(waitingFor-8).." "
		else
			status_message = status_message.."R"..tostring(waitingFor).."  "
		end
		if this.waitForWrite then
			status_message = status_message.."(W)"
		else
			status_message = status_message.."(R)"
		end
	elseif disassembler then
		status_message = "> "..disassembler.disassembleSingleInstruction(this.ir)
	end
	ioflags = {}
	for i=8,15 do
		local f = ""
		if tisNodeCanReadRegister(this,i)  then f = "R" else f = " " end
		if tisNodeCanWriteRegister(this,i) then f = f.."W" else f = f.." " end
		ioflags[i] = f
	end
	out =      string.format("┏━━━━━━━━━━━━━━━━━━━━━┓\n")
	out = out..string.format("┃ %-19s ┃\n",title:sub(1,19))
	out = out..string.format("┃ %-19s ┃\n",status_message)
	out = out..string.format("┣━━━━━━━━━━━━┳━━━━━━━━┫\n")
	out = out..string.format("┃ ACC 0x%04x ┃ IO0 %s ┃\n",this.registers[1],ioflags[8])
	out = out..string.format("┃ R2  0x%04x ┃ IO1 %s ┃\n",this.registers[2],ioflags[9])
	out = out..string.format("┃ R3  0x%04x ┃ IO2 %s ┃\n",this.registers[3],ioflags[10])
	out = out..string.format("┃ R4  0x%04x ┃ IO3 %s ┃\n",this.registers[4],ioflags[11])
	out = out..string.format("┃ R5  0x%04x ┃ IO4 %s ┃\n",this.registers[5],ioflags[12])
	out = out..string.format("┣━━━━━━━━━━━━┫ IO5 %s ┃\n",ioflags[13])
	out = out..string.format("┃ PC  0x%04x ┃ IO6 %s ┃\n",this.pc,ioflags[14])
	out = out..string.format("┃ IR  0x%04x ┃ IO7 %s ┃\n",this.ir,ioflags[15])
	out = out..string.format("┗━━━━━━━━━━━━┻━━━━━━━━┛\n")
	return out
end

return tisNodeRenderUtf8
